package com.example.demo.web.filter;

import com.example.demo.entity.Profile;
import com.example.demo.service.ProfileService;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public class RootFilterExample implements Filter {

    public RootFilterExample(ProfileService profileDBService){
        this.profileDBService = profileDBService;
    }

    private ProfileService profileDBService;

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain filterChain) throws IOException, ServletException {
        HttpSession httpSession = ((HttpServletRequest) request).getSession(true);

        //System.out.println("doRootFiler");

        Object idObj = httpSession.getAttribute("id");
        boolean checkRoot = false;
        if(idObj != null){
            Long id = Long.parseLong(idObj.toString());
            try {
                Profile profile = profileDBService.find1ByID(id);
                if(profile.rightExists("root"))
                    checkRoot = true;

            }catch (Exception e){
                System.err.println(getClass() + ": " + e);
            }
        }
        if(!checkRoot)
            ((HttpServletResponse) response).sendRedirect("/");
        filterChain.doFilter(request, response);
    }


}
