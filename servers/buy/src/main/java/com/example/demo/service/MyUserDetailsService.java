package com.example.demo.service;

import com.example.demo.entity.Profile;
import com.example.demo.entity.MyUserPrincipal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class MyUserDetailsService implements UserDetailsService {

    @Autowired
    private ProfileService profileDBService;


    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        Profile profile = profileDBService.find1ByName(username);
        if(profile == null)
            throw new UsernameNotFoundException(username);

        return new MyUserPrincipal(profile);
    }
}
