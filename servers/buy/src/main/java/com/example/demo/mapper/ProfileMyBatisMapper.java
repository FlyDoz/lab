package com.example.demo.mapper;


import com.example.demo.entity.Profile;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;


@Mapper
public interface ProfileMyBatisMapper {

    List<Profile> findAll();
    List<Profile> findAllByID(@Param("id") Long id);
    List<Profile> findAllByEmail(@Param("email") String email);
    List<Profile> findAllByName(@Param("name") String name);

    List<String> findAllRights();
    List<String> findRightByName(@Param("name") String name);

    void save(@Param("profile") Profile profile);

    void update(@Param("profile") Profile profile);

    void deleteAll();
    void deleteByID(@Param("id") Long id);
    void deleteByEmail(@Param("email") String email);

}
