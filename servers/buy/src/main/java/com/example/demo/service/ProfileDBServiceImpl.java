package com.example.demo.service;

import com.example.demo.entity.Profile;
import com.example.demo.mapper.ProfileMyBatisMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class ProfileDBServiceImpl implements ProfileService {

    @Autowired
    private PasswordEncoder passwordEncoder;

    private final ProfileMyBatisMapper profileMyBatisMapper;

    public ProfileDBServiceImpl(ProfileMyBatisMapper profileMyBatisMapper) {
        this.profileMyBatisMapper = profileMyBatisMapper;
    }


    @Override
    public List<Profile> findAll() {
        return profileMyBatisMapper.findAll();
    }

    @Override
    public List<Profile> findAllByID(Long id) {

        return profileMyBatisMapper.findAllByID(id);
    }

    @Override
    public List<Profile> findAllByEmail(String email) {
        return profileMyBatisMapper.findAllByEmail(email);
    }

    @Override
    public List<Profile> findAllByName(String name) {
        return profileMyBatisMapper.findAllByName(name);
    }

    @Override
    public List<String> findAllRights() {
        List<String> rights = profileMyBatisMapper.findAllRights();
        for (String item: rights)
            item = "ROLE_"+item;
        return rights;
    }

    @Override
    public Profile find1ByID(Long id) {
        List<Profile> profiles = this.findAllByID(id);
        if(profiles != null && profiles.size()>0)
            return profiles.get(0);
        else return null;
    }

    @Override
    public Profile find1ByEmail(String email) {
        List<Profile> profiles = this.findAllByEmail(email);
        if(profiles != null && profiles.size()>0)
            return profiles.get(0);
        else return null;
    }

    @Override
    public Profile find1ByName(String name) {
        List<Profile> profiles = this.findAllByName(name);
        if(profiles != null && profiles.size()>0)
            return profiles.get(0);
        else return null;
    }

    @Override
    public String find1RightsByName(String name) {
        List<String> names = profileMyBatisMapper.findRightByName(name);
        if(names != null)
            if(names.size() > 0)
                return profileMyBatisMapper.findRightByName(name).get(0);
        return null;
    }

    @Override
    public boolean profileExistsByID(Long id) {
        return this.find1ByID(id) != null;
    }

    @Override
    public boolean profileExistsByEmail(String email) {
        return this.find1ByEmail(email) != null;
    }

    @Override
    public boolean profileExistsByName(String name) {
        return this.find1ByName(name) != null;
    }

    @Override
    public void create(Profile profile) {

        profile.setPassword(passwordEncoder.encode(profile.getPassword()));

        profileMyBatisMapper.save(profile);
    }

    @Override
    public void update(Profile profile) {
        profileMyBatisMapper.update(profile);
    }

    @Override
    public void deleteAll() {
        profileMyBatisMapper.deleteAll();
    }

    @Override
    public void deleteByID(Long id) {
        profileMyBatisMapper.deleteByID(id);
    }

    @Override
    public void deleteByEmail(String email) {
        profileMyBatisMapper.deleteByEmail(email);
    }


    /*private Set<String> ROLE_rights(Set<String> rights){
        Set<String> new_rights = new HashSet<>();
        for (String item: rights)
            item = "ROLE_"+item;
        return rights;
    }*/


}
