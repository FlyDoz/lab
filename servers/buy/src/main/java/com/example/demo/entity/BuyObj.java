package com.example.demo.entity;

import lombok.Data;

import java.sql.Date;

@Data
public class BuyObj {
    private Long id;
    private String material;
    private String shop;
    private int amount;
    private float money;
    private Date date;


    public BuyObj(){}
    public BuyObj(Long id, String material, String shop, int amount, float money, Date date){
        this.id = id;
        this.material = material;
        this.shop = shop;
        this.amount = amount;
        this.money = money;
        this.date = date;
    }
    public BuyObj(String material, String shop, int amount, float money, Date date){
        this.material = material;
        this.shop = shop;
        this.amount = amount;
        this.money = money;
        this.date = date;
    }
}
