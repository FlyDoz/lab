package com.example.demo.entity;

import com.example.demo.entity.Profile;
import lombok.Data;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

@Data
public class MyUserPrincipal implements UserDetails {

    private Profile profile;

    private boolean accountNonExpired = true;
    private boolean accountNonLocked = true;
    private boolean credentialsNonExpired = true;
    private boolean enabled = true;

    public MyUserPrincipal(Profile profile){
        Set<String> ROLE_rights = new HashSet<>();
        for (String item: profile.getRights())
            ROLE_rights.add("ROLE_" + item);

        this.profile = new Profile(profile, ROLE_rights);;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        Collection<GrantedAuthority> authorities = new HashSet<>();
        for (String item: profile.getRights())
            authorities.add(new SimpleGrantedAuthority(item));
        return authorities;
    }

    @Override
    public String getPassword() {
        return profile.getPassword();
    }

    @Override
    public String getUsername() {
        return profile.getName();
    }

    @Override
    public boolean isAccountNonExpired() {
        return accountNonExpired;//TODO ?
    }

    @Override
    public boolean isAccountNonLocked() {
        return accountNonLocked;//TODO ?
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return credentialsNonExpired;//TODO ?
    }

    @Override
    public boolean isEnabled() {
        return enabled;//TODO ?
    }
}
