<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<%@ attribute name="obj" type="java.lang.String"%>
<%@ attribute name="field" type="java.lang.String"%>
<%--
  Created by IntelliJ IDEA.
  User: User
  Date: 30.01.2020
  Time: 15:08
  To change this template use File | Settings | File Templates.
--%>
<tr><td/><td>
    <spring:hasBindErrors name="${obj}" htmlEscape="true">
        <c:if test="${errors.errorCount gt 0}">
            <c:forEach items="${errors.getFieldErrors(field)}" var="error">
                <spring:message code="${error.defaultMessage}"/><br/>
            </c:forEach>
        </c:if>
    </spring:hasBindErrors>
</td></tr>
