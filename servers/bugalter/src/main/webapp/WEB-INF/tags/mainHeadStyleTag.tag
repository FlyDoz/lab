<%--
  Created by IntelliJ IDEA.
  User: User
  Date: 05.02.2020
  Time: 9:17
  To change this template use File | Settings | File Templates.
--%>
<%@ tag description="Simple Wrapper Tag" pageEncoding="UTF-8"%>


<head>
    <style type="text/css">
        .backGround{
            background: lightgray;
            width: 800px;
            height: 100%;
        }
        .paddyng25 {
            padding: 25px;
        }
        .topMenu{
            background: darkgray;
            position: relative;
            height: 100px;
        }
        .topMenuButtons{
            background: darkgray;
            position: absolute;
            bottom: 0px;
            display: flex;
        }
        .topMenuButton{
            background: black;
            color: white;
            border-width: 0px;
            font-size: 18px;
            margin-right: 1px;
        }
        .topMenuExit{
            position: absolute;
            top: 0px;
            right: 0px;
        }
        .copyrightMenu{
            background: darkgray;
            position: fixed;
            bottom: 0px;
            width: 800px;
        }
        .formHeight{
            height: 0px;
        }
        @font-face {
            font-family: pixelStyle;
            src: url('/font/16803.ttf');
        }
        .topMenuFontHeader{
            font-size: 80px;
            font-family: "pixelStyle";
        }
        .myGif{
            position: absolute;
            top: 0px;
            left: -100px;
            width: 100px;
        }
        .width200{
            width: 200px;
        }
        .width650{
            width: 650px;
        }
        .x{
            background-color: Transparent;
            background-repeat:no-repeat;
            border: none;
            cursor:pointer;
            overflow: hidden;
        }
    </style>

</head>

