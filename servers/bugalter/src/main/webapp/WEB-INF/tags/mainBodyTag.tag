<%@tag description="Simple Wrapper Tag" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>



<t:mainHeadStyleTag/>



<body topmargin="0px" bottommargin="0px" bgcolor="#fafafa">

    <div align="center"><div class="backGround">
        <div align="right" class="topMenu">

            <sec:authorize access="isAuthenticated()">
                <div class="topMenuButtons">
                    <form id="simpleForm" method="get" class="formHeight"></form>
                        <button form="simpleForm" formaction="/" type="submit" class="topMenuButton"><spring:message code="main" /></button>
                        <button form="simpleForm" formaction="/list_profiles" type="submit" class="topMenuButton"><spring:message code="users" /></button>
                </div>
            </sec:authorize>

            <div align="left">
                <span class="topMenuFontHeader">
                    <spring:message code="header_top" />
                </span>
            </div>
            <img src="/img/gif/MAIN.gif" class="myGif"/>

            <div class="topMenuExit">
                <t:exitButtonTag/>
                <t:languageTag />
            </div>


        </div>

        <div class="copyrightMenu">
            © FlyDoz
        </div>

        <div class="paddyng25">
            <jsp:doBody/>
        </div>
    </div></div>
</body>