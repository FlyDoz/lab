<%@ tag description="Simple Wrapper Tag" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<head>
    <style type="text/css">
        ul{
            display: block;
            margin: 0;
            padding: 0;
            list-style: none;
        }
        ul:after{
            display: block;
            content: ' ';
            clear: both;
            float: none;
        }
        ul.menu > li{
            float: right;
            position: relative;
        }
        ul.menu > li > a {
            display: block;
            /*padding: 10px;*/
            color: white;
            background-color: red;
            text-decoration: none;
        }
        ul.menu > li > a:hover {
            background-color: black;
        }
        ul.submenu {
            display: none;
            position: absolute;
            /*width: 120px;*/
            /*top: 0px;*/
            left: 0;
            background-color: white;
            border: 1px solid red;
        }
        ul.submenu > li {
            display: block;
        }
        ul.submenu > li > a {
            display: block;
            /*padding: 10px;*/
            color: white;
            background-color: red;
            text-decoration: none;
        }
        ul.submenu > li > a:hover {
            /*text-decoration: underline;*/
        }
        ul.menu > li:hover > ul.submenu {
            display: block;
        }
        /*.fit-picture {
            width: 20px;
        }*/
    </style>
</head>



<body>

    <jsp:doBody/>

    <ul class="menu">
        <li>
            <c:if test="${pageContext.response.locale.getLanguage().equals('ru')}">
                <img class="fit-picture" src="/img/bmp/ru.bmp" alt="${language}">
            </c:if>
            <c:if test="${pageContext.response.locale.getLanguage().equals('en')}">
                <img class="fit-picture" src="/img/bmp/en.bmp" alt="${language}">
            </c:if>
            <ul class="submenu">
                <li>
                    <form method="post" action="/changeLocalization">
                        <input type="image" name="submit" src="/img/bmp/ru.bmp" alt="ru">
                        <input type="hidden" name="changeLanguage" value="ru">
                    </form>
                </li>
                <li>
                    <form method="post" action="/changeLocalization">
                        <input type="image" name="submit" src="/img/bmp/en.bmp" alt="en">
                        <input type="hidden" name="changeLanguage" value="en">
                    </form>
                </li>
            </ul>
        </li>
        <%--<li><a href=#>Menu 2</a>
            <ul class="submenu">
                <li><a href=#>Sudmenu 2</a></li>
                <li><a href=#>Sudmenu 2</a></li>
            </ul>
        </li>--%>
    </ul>
</body>