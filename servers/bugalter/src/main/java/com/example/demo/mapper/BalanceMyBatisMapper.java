package com.example.demo.mapper;

import com.example.demo.entity.BalanceOfMoney;
import com.example.demo.entity.BuyObj;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface BalanceMyBatisMapper {

    BalanceOfMoney read();
    void sell(@Param("money") float money);
    void buy(@Param("money") float money);
    void sell2(@Param("obj")BuyObj buyObj);
    void buy2(@Param("obj")BuyObj buyObj);

}
