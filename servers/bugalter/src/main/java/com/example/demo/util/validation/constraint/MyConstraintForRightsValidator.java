package com.example.demo.util.validation.constraint;

import com.example.demo.service.ProfileService;
import org.springframework.beans.factory.annotation.Autowired;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Set;



public class MyConstraintForRightsValidator implements ConstraintValidator<MyConstraintForRights, Set<String>> {

    @Autowired
    private ProfileService profileDBService;

    @Override
    public boolean isValid(Set<String> rights, ConstraintValidatorContext constraintValidatorContext) {

        if(rights != null) {
            for (String item : rights)
                if (item != null) {
                    try {
//                        System.out.println(getClass() + ": " + item + " " + profileDBService.find1RightsByName(item));
                        if (profileDBService.find1RightsByName(item) == null)
                            return false;
                    }catch (Exception e){
                        System.err.println(getClass() + ": flydoz: " + e + " but item = " + item + " and profileDBService = " + profileDBService + " and profileDBService ?== null - " + (profileDBService == null));
                        return false;
                    }
                } else return false;
        }else return false;

        return true;
    }
}
