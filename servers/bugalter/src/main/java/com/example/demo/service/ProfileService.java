package com.example.demo.service;

import com.example.demo.entity.Profile;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.List;


public interface ProfileService {
    List<Profile> findAll();
    List<Profile> findAllByID(Long id);
    List<Profile> findAllByEmail(String email);
    List<Profile> findAllByName(String name);

    List<String> findAllRights();

    Profile find1ByID(Long id);
    Profile find1ByEmail(String email);
    Profile find1ByName(String name);//имена могут повторяться в моей базе данных, уникальные только id и email

    String find1RightsByName(String name);


    boolean profileExistsByID(Long id);
    boolean profileExistsByEmail(String email);
    boolean profileExistsByName(String name);


    void create(Profile profile);
    void update(Profile profile);


    void deleteAll();
    void deleteByID(Long id);
    void deleteByEmail(String email);
}
