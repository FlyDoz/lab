package com.example.demo.service;

import com.example.demo.entity.BalanceOfMoney;
import com.example.demo.entity.BuyObj;
import com.example.demo.mapper.BalanceMyBatisMapper;
import org.springframework.stereotype.Service;

@Service
public class BalanceServiceImpl implements BalanceService {


    private final BalanceMyBatisMapper balanceMyBatisMapper;

    public BalanceServiceImpl(BalanceMyBatisMapper balanceMyBatisMapper) {
        this.balanceMyBatisMapper = balanceMyBatisMapper;
    }

    @Override
    public BalanceOfMoney read() {
        return balanceMyBatisMapper.read();
    }

    @Override
    public void sell(float money) {
        balanceMyBatisMapper.sell(money);
    }

    @Override
    public void buy(float money) {
        balanceMyBatisMapper.buy(money);
    }

    @Override
    public void sell(BuyObj buyObj) {
        balanceMyBatisMapper.sell(buyObj.getMoney());
        balanceMyBatisMapper.sell2(buyObj);
    }

    @Override
    public void buy(BuyObj buyObj) {
        balanceMyBatisMapper.buy(buyObj.getMoney());
        balanceMyBatisMapper.buy2(buyObj);
    }
}
