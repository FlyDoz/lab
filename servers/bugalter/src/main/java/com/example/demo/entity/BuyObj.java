package com.example.demo.entity;

import lombok.Data;

import java.sql.Date;

@Data
public class BuyObj {
    private Long id;
    private String name;
    private float money;
    private Date date;

    public BuyObj(){}
    public BuyObj(Long id, String name, float money, Date date){
        this.id = id;
        this.name = name;
        this.money = money;
        this.date = date;
    }
    public BuyObj(String name, float money, Date date){
        this.name = name;
        this.money = money;
        this.date = date;
    }

}
