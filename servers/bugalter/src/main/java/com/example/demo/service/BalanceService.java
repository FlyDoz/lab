package com.example.demo.service;

import com.example.demo.entity.BalanceOfMoney;
import com.example.demo.entity.BuyObj;

public interface BalanceService {
    BalanceOfMoney read();
    void sell(float money);
    void buy(float money);
    void sell(BuyObj buyObj);
    void buy(BuyObj buyObj);

}
