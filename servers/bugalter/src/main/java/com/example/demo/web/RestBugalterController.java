package com.example.demo.web;


import com.example.demo.entity.BalanceOfMoney;
import com.example.demo.entity.BuyObj;
import com.example.demo.service.BalanceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;

@RestController
@RequestMapping("/rest")
public class RestBugalterController {

    @Autowired
    private BalanceService balanceDBService;

    @RequestMapping(value = "/balance", method = RequestMethod.GET)
    public BalanceOfMoney balance(HttpServletRequest request, HttpServletResponse response, HttpSession httpSession, Locale locale){

        response.setHeader("Access-Control-Allow-Origin", "*");
        response.setHeader("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE");
        response.setHeader("Access-Control-Max-Age", "3600");
        response.setHeader("Access-Control-Allow-Headers", "x-requested-with");

        String requestMethod = request.getParameter("RequestMethod");
        if(requestMethod != null){
            if(requestMethod.equals("UPDATE")) {
                String buy = request.getParameter("buyMoney");//TODO в /buy
                if (buy != null)
                    try {
                        balanceDBService.buy(Float.parseFloat(buy));
                    } catch (Exception e) {}

                String sell = request.getParameter("sellMoney");//TODO в /sell
                if(sell != null)
                    try {
                        balanceDBService.sell(Float.parseFloat(sell));
                    } catch (Exception e) {}
            }
        }else{
            System.out.println("requestMethod == null");
        }

        return balanceDBService.read();
    }

    @RequestMapping(value = "/buy", method = RequestMethod.GET)
    public BalanceOfMoney buy(HttpServletRequest request, HttpServletResponse response, HttpSession httpSession, Locale locale){

        response.setHeader("Access-Control-Allow-Origin", "*");
        response.setHeader("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE");
        response.setHeader("Access-Control-Max-Age", "3600");
        response.setHeader("Access-Control-Allow-Headers", "x-requested-with");


        String money = request.getParameter("money");
        String name = request.getParameter("name");

        java.sql.Date sqlDate = null;
        try{
            String date = request.getParameter("date");
            java.util.Date utilDate = new SimpleDateFormat("yyyy-MM-dd").parse(date);
            sqlDate = new java.sql.Date(utilDate.getTime());
        }catch (Exception e){}

        if (money != null)
            try {
                balanceDBService.buy(new BuyObj(name, Float.parseFloat(money), sqlDate));
            } catch (Exception e) {}

        return balanceDBService.read();
    }

    @RequestMapping(value = "/sell", method = RequestMethod.GET)
    public BalanceOfMoney sell(HttpServletRequest request, HttpServletResponse response, HttpSession httpSession, Locale locale){

        response.setHeader("Access-Control-Allow-Origin", "*");
        response.setHeader("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE");
        response.setHeader("Access-Control-Max-Age", "3600");
        response.setHeader("Access-Control-Allow-Headers", "x-requested-with");


        String money = request.getParameter("money");
        String name = request.getParameter("name");

        java.sql.Date sqlDate = null;
        try{
            String date = request.getParameter("date");
            java.util.Date utilDate = new SimpleDateFormat("yyyy-MM-dd").parse(date);
            sqlDate = new java.sql.Date(utilDate.getTime());
        }catch (Exception e){}

        if (money != null)
            try {
                balanceDBService.sell(new BuyObj(name, Float.parseFloat(money), sqlDate));
            } catch (Exception e) {}

        return balanceDBService.read();
    }

}
