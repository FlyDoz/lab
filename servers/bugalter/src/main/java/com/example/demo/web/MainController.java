package com.example.demo.web;

import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;

@RestController
public class MainController {

    @GetMapping("/signin")
    public ModelAndView showSignInForm() {
        return new ModelAndView("signIn");
    }


    @GetMapping("/")
    public ModelAndView showIndex(HttpSession httpSession){
        return new ModelAndView("index");
    }
    @GetMapping("/welcome")
    public ModelAndView showIndex2(HttpSession httpSession){
        return new ModelAndView("index");
    }

}
