package com.example.demo.entity;

import lombok.Data;

@Data
public class BalanceOfMoney {
    private Long id;
    private String name;
    private float money;
}
