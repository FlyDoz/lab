package com.example.demo.web;

import com.example.demo.entity.Profile;
import com.example.demo.service.ProfileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;

@RestController
public class ListProfilesController {

    @Autowired
    private ProfileService profileService;


    @RequestMapping(value = "/list_profiles", method = RequestMethod.GET)
    public ModelAndView findAllProfiles(){
        return new ModelAndView("listProfiles", "listOfProfiles", profileService.findAll());
    }


    @RequestMapping(value = "/list_profiles", method = RequestMethod.POST)
    public ModelAndView findAllProfilesBySomething(HttpServletRequest request){
        String email = request.getParameter("findByEmail");
        if(email.length()>0)
            return new ModelAndView("listProfiles").addObject("listOfProfiles", profileService.findAllByEmail(email)).addObject("profile", new Profile());

        return new ModelAndView("listProfiles").addObject("listOfProfiles", profileService.findAll()).addObject("profile", new Profile());
    }
}
