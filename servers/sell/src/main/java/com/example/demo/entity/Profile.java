package com.example.demo.entity;

import com.example.demo.util.validation.constraint.MyConstraintForRights;
import lombok.Data;


import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;


@Data
public class Profile {

//    public String toString() {
//        return "Profile";
//    }


    private Long id;

    @Size(min=1,max=40, message = "nameBadSize")
    @NotNull(message = "nameIsNull")
    private String name;

    @Size(min=1,max=60, message = "passwordBadSize")
    @NotNull(message = "passwordIsNull")
    private String password;

    @Pattern(regexp = "^[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?$", message="emailSyntaxError")
    @NotNull(message = "emailIsNull")
    //@TODO проверка существования аккаунта, может быть добавить
    private String email;

    /*@Null*///TODO проверить
    @MyConstraintForRights(message = "somerRightNotExists")
    private Set<String> rights;

    public String getRightsAsStringLine() {
        String StringLine =  "";
        for (String item:rights)
            StringLine += item + (" ");
        return StringLine.substring(0,StringLine.length()-1);
    }

    public Profile(){}
    public Profile(Profile profile){
        id = profile.getId();
        name = profile.getName();
        password = profile.getPassword();
        email = profile.getEmail();

        rights = new HashSet<>();
        for (String item: profile.getRights())
            rights.add(item);
    }
    public Profile(Profile profile, Set<String> rights){
        id = profile.getId();
        name = profile.getName();
        password = profile.getPassword();
        email = profile.getEmail();

        this.rights = rights;
    }

    public Profile(Long id, String name, String password, String email, Set<String> rights){
        this.id = id;
        this.name = name.replaceAll(" ", "");
        this.password = password.replaceAll(" ", "");
        this.email = email.replaceAll(" ", "");

        this.rights = new HashSet<>();
        for (String item: rights)
            this.rights.add(item.replaceAll(" ", ""));
    }
    public Profile(Long id, String name, String password, String email, String rights){
        this.id = id;
        this.name = name.replaceAll(" ", "");
        this.password = password.replaceAll(" ", "");
        this.email = email.replaceAll(" ", "");

        this.rights = new HashSet<>(Arrays.asList(rights.split(" ")));
    }
    public Profile(Long id, String name, String password, String email, Object rights){
        this.id = id;
        this.name = name.replaceAll(" ", "");
        this.password = password.replaceAll(" ", "");
        this.email = email.replaceAll(" ", "");

        this.rights = new HashSet<>(Arrays.asList(rights.toString().split(" ")));
    }
    public Profile(Long id, Object name, Object password, Object email, Object rights){
        this.id = id;
        this.name = name.toString().replaceAll(" ", "");
        this.password = password.toString().replaceAll(" ", "");
        this.email = email.toString().replaceAll(" ", "");

        this.rights = new HashSet<>(Arrays.asList(rights.toString().split(" ")));
    }

    public Profile(Object id, Object name, Object password, Object email, Object rights){
        this.id = Long.parseLong(id.toString());
        this.name = name.toString().replaceAll(" ", "");
        this.password = password.toString().replaceAll(" ", "");
        this.email = email.toString().replaceAll(" ", "");

        this.rights = new HashSet<>(Arrays.asList(rights.toString().split(" ")));
    }

    public Profile(String name, String password, String email, Set<String> rights){
        this.name = name.replaceAll(" ", "");
        this.password = password.replaceAll(" ", "");
        this.email = email.replaceAll(" ", "");

        this.rights = new HashSet<>();
        for (String item: rights)
            this.rights.add(item.replaceAll(" ", ""));
    }
    public Profile(String name, String password, String email, String rights){
        this.name = name.replaceAll(" ", "");
        this.password = password.replaceAll(" ", "");
        this.email = email.replaceAll(" ", "");

        this.rights = new HashSet<>(Arrays.asList(rights.split(" ")));
    }
    public Profile(String name, String password, String email, Object rights){
        this.name = name.replaceAll(" ", "");
        this.password = password.replaceAll(" ", "");
        this.email = email.replaceAll(" ", "");

        this.rights = new HashSet<>(Arrays.asList(rights.toString().split(" ")));
    }
    public Profile(Object name, Object password, Object email, Object rights){
        this.name = name.toString().replaceAll(" ", "");
        this.password = password.toString().replaceAll(" ", "");
        this.email = email.toString().replaceAll(" ", "");

        this.rights = new HashSet<>(Arrays.asList(rights.toString().split(" ")));
    }

    public boolean rightExists(String right){
        if(rights != null)
            for (String item:rights)
                if(right.equals(item))
                    return true;
        return false;
    }
}
