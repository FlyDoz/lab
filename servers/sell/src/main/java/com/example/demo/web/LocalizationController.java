package com.example.demo.web;

import org.apache.tomcat.jni.Local;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.i18n.SessionLocaleResolver;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.swing.plaf.nimbus.AbstractRegionPainter;
import java.util.Enumeration;
import java.util.Locale;
import java.util.Map;

@RestController
public class LocalizationController {

    @Autowired
    private SessionLocaleResolver localeResolver;

    private static String[] patterns = new String[]{
            "ru",
            "en",
    };

    @RequestMapping(value = "/changeLocalization", method = RequestMethod.POST)
    public ModelAndView change(HttpServletRequest request, HttpServletResponse response, HttpSession httpSession, Local local){

        String changeLanguage = request.getParameter("changeLanguage");
        if(changeLanguage != null)
            for (String item:patterns)
                if(item.equals(changeLanguage))
                    localeResolver.setLocale(request, response, new Locale(changeLanguage));

        return new ModelAndView("redirect:/");
    }
}
