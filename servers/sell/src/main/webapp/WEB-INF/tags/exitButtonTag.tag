<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%--
  Created by IntelliJ IDEA.
  User: User
  Date: 05.02.2020
  Time: 12:02
  To change this template use File | Settings | File Templates.
--%>
<%@tag description="Simple Wrapper Tag" pageEncoding="UTF-8"%>


<sec:authorize access="isAuthenticated()">
    <form method="post" action="/exit">
        <input type="submit" value="<spring:message code="exit" />"/>
    </form>
</sec:authorize>

