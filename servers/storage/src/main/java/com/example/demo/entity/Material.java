package com.example.demo.entity;

import lombok.Data;

@Data
public class Material {

    private Long id;
    private String name;
    private int amount;/*количество*/


    public Material(){}
    public Material(Long id, String name, int amount){
        this.id = id;
        this.name = name;
        this.amount = amount;
    }
    public Material(String name, int amount){
        this.name = name;
        this.amount = amount;
    }
}
