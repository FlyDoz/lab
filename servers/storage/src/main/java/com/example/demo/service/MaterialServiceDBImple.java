package com.example.demo.service;


import com.example.demo.entity.Material;
import com.example.demo.mapper.MaterialsMyBatisMapper;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MaterialServiceDBImple implements ItemService<Material> {


    private final MaterialsMyBatisMapper profileMyBatisMapper;

    public MaterialServiceDBImple(MaterialsMyBatisMapper profileMyBatisMapper) {
        this.profileMyBatisMapper = profileMyBatisMapper;
    }


    @Override
    public List<Material> findAll() {
        return profileMyBatisMapper.findAll();
    }
    @Override
    public List<Material> findById(Long id) {
        return profileMyBatisMapper.findById(id);
    }
    @Override
    public void create(Material material) {
        profileMyBatisMapper.create(material);
    }
    @Override
    public void update(Material material){
        profileMyBatisMapper.update(material);
    }
    @Override
    public void delete(Long id){
        profileMyBatisMapper.delete(id);
    }
    @Override
    public void sellUpdate(int amount, Long id){
        profileMyBatisMapper.sellUpdate(amount, id);
    }
}
