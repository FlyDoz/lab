package com.example.demo.entity;

import lombok.Data;

@Data
public class Product {

    private Long id;
    private String name;
    private int amount;/*количество*/

    public Product(){}
    public Product(Long id, String name, int amount){
        this.id = id;
        this.name = name;
        this.amount = amount;
    }
    public Product(String name, int amount){
        this.name = name;
        this.amount = amount;
    }
}
