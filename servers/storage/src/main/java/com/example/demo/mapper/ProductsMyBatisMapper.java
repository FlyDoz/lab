package com.example.demo.mapper;






import com.example.demo.entity.Product;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;


@Mapper
public interface ProductsMyBatisMapper {

    List<Product> findAll();
    List<Product> findById(@Param("id") Long id);

    void create(@Param("product") Product product);
    void update(@Param("product") Product product);
    void sellUpdate(@Param("amount") int amount, @Param("id") Long id);
    void addUpdate(@Param("amount") int amount, @Param("id") Long id);
    void delete(@Param("id") Long id);
}
