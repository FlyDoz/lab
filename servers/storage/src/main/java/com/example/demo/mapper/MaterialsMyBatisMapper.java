package com.example.demo.mapper;





import com.example.demo.entity.Material;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;


@Mapper
public interface MaterialsMyBatisMapper {

    List<Material> findAll();
    List<Material> findById(@Param("id") Long id);

    void create(@Param("material") Material material);
    void update(@Param("material") Material material);
    void sellUpdate(@Param("amount") int amount, @Param("id") Long id);
    void delete(@Param("id") Long id);
}
