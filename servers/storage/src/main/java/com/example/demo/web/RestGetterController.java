package com.example.demo.web;

import com.example.demo.entity.Material;
import com.example.demo.entity.Product;
import com.example.demo.service.MaterialServiceDBImple;
import com.example.demo.service.ProductServiceDBImple;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;

@RestController
@RequestMapping("/rest")
public class RestGetterController {
    @Autowired
    private MaterialServiceDBImple materialServiceDBImple;
    @Autowired
    private ProductServiceDBImple productServiceDBImple;


    @RequestMapping(value = "/materials", method = RequestMethod.GET)
    public List<Material> materials(HttpServletRequest request, HttpServletResponse response, HttpSession httpSession, Locale locale){

        response.setHeader("Access-Control-Allow-Origin", "*");
        response.setHeader("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE");
        response.setHeader("Access-Control-Max-Age", "3600");
        response.setHeader("Access-Control-Allow-Headers", "x-requested-with");

        String requestMethod = request.getParameter("RequestMethod");
        if(requestMethod != null){
            if(requestMethod.equals("GET")) materialsGet(request,httpSession,locale);
            if(requestMethod.equals("POST")) materialsPost(request,httpSession,locale);
            if(requestMethod.equals("UPDATE")) materialsUpdate(request,httpSession,locale);
            if(requestMethod.equals("DELETE")) materialsDelete(request,httpSession,locale);
        }else{
            System.out.println("requestMethod == null");
        }

        return materialServiceDBImple.findAll();
    }


    @RequestMapping(value = "/products", method = RequestMethod.GET)
    public List<Product> findAllProducts(HttpServletRequest request, HttpServletResponse response, HttpSession httpSession, Locale locale){

        System.out.println("try to do something");

        response.setHeader("Access-Control-Allow-Origin", "*");
        response.setHeader("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE");
        response.setHeader("Access-Control-Max-Age", "3600");
        response.setHeader("Access-Control-Allow-Headers", "x-requested-with");

        String requestMethod = request.getParameter("RequestMethod");
        if(requestMethod != null){
            if(requestMethod.equals("GET")) productsGet(request,httpSession,locale);
            if(requestMethod.equals("POST")) productsPost(request,httpSession,locale);
            if(requestMethod.equals("UPDATE")) productsUpdate(request,httpSession,locale);
            if(requestMethod.equals("DELETE")) productsDelete(request,httpSession,locale);
        }else{
            System.out.println("requestMethod == null");
        }

        return productServiceDBImple.findAll();//TODO доделать
    }


    private List<Material> materialsGet(HttpServletRequest request, HttpSession httpSession, Locale locale){
        String sId = request.getParameter("id");
        if(sId != null)
            try {
                Long id = Long.parseLong(sId);
                return materialServiceDBImple.findById(id);
            }catch (Exception e){}

        return materialServiceDBImple.findAll();
    }
    private List<Material> materialsPost(HttpServletRequest request, HttpSession httpSession, Locale locale){
        String sName = request.getParameter("name");
        String sAmount = request.getParameter("amount");
        System.out.println("materialsPost");
        if(sName != null && sAmount != null)
            try {
                System.out.println("materialsPost try");
                int amount = Integer.parseInt(sAmount);
                materialServiceDBImple.create(new Material(sName, amount));
                System.out.println("materialsPost sucsess");
            }catch (Exception e){
                System.out.println(e);
            }
        return materialServiceDBImple.findAll();
    }
    private List<Material> materialsUpdate(HttpServletRequest request, HttpSession httpSession, Locale locale){
        String sId = request.getParameter("id");
        String sName = request.getParameter("name");
        String sAmount = request.getParameter("amount");
        if(sId != null && sName != null && sAmount != null)
            try {
                Long id = Long.parseLong(sId);
                int amount = Integer.parseInt(sAmount);
                materialServiceDBImple.update(new Material(id, sName, amount));

            }catch (Exception e){}

        String TypeUpdate = request.getParameter("TypeUpdate");
        String sellAmount = request.getParameter("sellAmount");
        if(sId != null && TypeUpdate != null && sellAmount != null)
            try {
                Long id = Long.parseLong(sId);
                //System.out.println("TypeUpdate = " + TypeUpdate + "  sellAmount = " + sellAmount + "  sId = " + sId);
                if(TypeUpdate.equals("sell"))
                    materialServiceDBImple.sellUpdate(Integer.parseInt(sellAmount), id);
            }catch (Exception e){}


        return materialServiceDBImple.findAll();
    }
    private List<Material> materialsDelete(HttpServletRequest request, HttpSession httpSession, Locale locale){
        String sId = request.getParameter("id");
        if(sId != null)
            try {
                Long id = Long.parseLong(sId);
                materialServiceDBImple.delete(id);
            }catch (Exception e){}
        return materialServiceDBImple.findAll();
    }




    private List<Product> productsGet(HttpServletRequest request, HttpSession httpSession, Locale locale){
        String sId = request.getParameter("id");
        if(sId != null)
            try {
                Long id = Long.parseLong(sId);
                return productServiceDBImple.findById(id);
            }catch (Exception e){}

        return productServiceDBImple.findAll();
    }
    private List<Product> productsPost(HttpServletRequest request, HttpSession httpSession, Locale locale){
        String sName = request.getParameter("name");
        String sAmount = request.getParameter("amount");

        System.out.println("productsPost");
        if(sName != null && sAmount != null)
            try {
                System.out.println("try productsPost");
                int amount = Integer.parseInt(sAmount);
                productServiceDBImple.create(new Product(sName, amount));
                System.out.println("try productsPost sucsess");
            }catch (Exception e){}
        return productServiceDBImple.findAll();
    }
    private List<Product> productsUpdate(HttpServletRequest request, HttpSession httpSession, Locale locale){
        String sId = request.getParameter("id");
        String sName = request.getParameter("name");
        String sAmount = request.getParameter("amount");
        if(sId != null && sName != null && sAmount != null)
            try {
                Long id = Long.parseLong(sId);
                int amount = Integer.parseInt(sAmount);
                productServiceDBImple.update(new Product(id, sName, amount));

            }catch (Exception e){}

        String TypeUpdate = request.getParameter("TypeUpdate");
        String sellAmount = request.getParameter("sellAmount");
        if(sId != null && TypeUpdate != null && sellAmount != null)
            try {
                Long id = Long.parseLong(sId);
                //System.out.println("TypeUpdate = " + TypeUpdate + "  sellAmount = " + sellAmount + "  sId = " + sId);
                if(TypeUpdate.equals("sell"))
                    productServiceDBImple.sellUpdate(Integer.parseInt(sellAmount), id);
            }catch (Exception e){}


        return productServiceDBImple.findAll();
    }
    private List<Product> productsDelete(HttpServletRequest request, HttpSession httpSession, Locale locale){
        String sId = request.getParameter("id");
        if(sId != null)
            try {
                Long id = Long.parseLong(sId);
                productServiceDBImple.delete(id);
            }catch (Exception e){}
        return productServiceDBImple.findAll();
    }


}
