package com.example.demo.web;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Locale;
import java.util.ResourceBundle;

@RestController
@RequestMapping("/products")
public class ProductionController {


    @RequestMapping(value = "/table", method = RequestMethod.GET)
    public ModelAndView deleteProfileByRoot(HttpServletRequest request, HttpSession httpSession, Locale locale){
        return new ModelAndView("rest/products");//.addObject("error", ResourceBundle.getBundle("locales/messages", locale).getString("accauntNotDeleted"));
    }

}
