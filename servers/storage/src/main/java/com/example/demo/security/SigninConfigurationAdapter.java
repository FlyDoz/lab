package com.example.demo.security;

import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@Configuration
@Order(1)
public class SigninConfigurationAdapter extends WebSecurityConfigurerAdapter {

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http    .anonymous().and()
                .authorizeRequests()
                .antMatchers(HttpMethod.POST, "/changeLocalization**").permitAll()
                .antMatchers(HttpMethod.GET, "/img/**").permitAll()
                .antMatchers(HttpMethod.GET, "/font/**").permitAll()
                .antMatchers("/root/**").hasRole("root")
                .antMatchers("/viwer/**").hasAnyRole("root", "viwer")
                .antMatchers(HttpMethod.GET, "/rest/**").permitAll()
                .anyRequest().authenticated()



                .and()
                .formLogin()
                .loginPage("/signin")
                .loginProcessingUrl("/signin2")
                .failureUrl("/signin?error=signinError")
                .defaultSuccessUrl("/")
                .permitAll()

                .and()
                .logout()
                .logoutUrl("/exit")
                .logoutSuccessUrl("/signin")
                //
                //?
                .deleteCookies("JSESSIONID")


                .and()
                .csrf().disable()
        ;
    }


}
