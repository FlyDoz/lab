package com.example.demo.util.validation.constraint;


import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ElementType.METHOD, ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = MyConstraintForRightsValidator.class)
public @interface MyConstraintForRights {
    String message() default "somerRightNotExists";/*"{value.negative}";*/

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
