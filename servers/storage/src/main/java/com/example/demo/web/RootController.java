package com.example.demo.web;

import com.example.demo.entity.Profile;
import com.example.demo.service.ProfileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.*;
import java.util.Locale;
import java.util.ResourceBundle;

@RestController
@RequestMapping("/root")
public class RootController {

    @Autowired
    private ProfileService profileDBService;



    @RequestMapping(value = "/delete_profile", method = RequestMethod.POST)
    public ModelAndView deleteProfileByRoot(HttpServletRequest request, HttpSession httpSession, Locale locale){
        String idStr = request.getParameter("idOfProfile");

        if(idStr != null)
            try {
                profileDBService.deleteByID(Long.parseLong(idStr));
                return new ModelAndView("redirect:/list_profiles");
            }catch (Exception e){ System.err.println(e); }

        return new ModelAndView("redirect:/list_profiles").addObject("error", ResourceBundle.getBundle("locales/messages", locale).getString("accauntNotDeleted"));
    }



    @RequestMapping(value = "/profileFormCreate", method = RequestMethod.GET)
    public ModelAndView showCreateProfileForm() {
        return new ModelAndView("root/profileFormCreate", "profile", new Profile("name1", "password1","email@email.email","user root"));
    }



    @RequestMapping(value = "/profileFormUpdate", method = RequestMethod.GET)
    public ModelAndView showUpdateProfileForm(HttpServletRequest request) {
        String idOfProfileStr = request.getParameter("idOfProfile");
        if(idOfProfileStr != null){
            Profile profile = profileDBService.find1ByID(Long.parseLong(idOfProfileStr));
            return new ModelAndView("root/profileFormEdit", "profile", profile);
        }
        return new ModelAndView("redirect:/list_profiles").addObject("error", "id == null");
    }



    @RequestMapping(value = "/addProfile", method = RequestMethod.POST)
    public ModelAndView addProfile(@Valid @ModelAttribute("profile")Profile profile, BindingResult result,/*ModelMap model,*/Locale locale, HttpSession httpSession) {

        if(profileDBService.find1ByEmail(profile.getEmail()) != null)
            result.addError(new FieldError("profile", "email", "emailAlreadyTaken"));

        if (result.hasErrors())
            return new ModelAndView("root/profileFormCreate", "profile", profile);;

        //profile.setPassword(passwordEncoder.encode(profile.getPassword()));

        profileDBService.create(profile);

        return new ModelAndView("redirect:/list_profiles");
    }



    @RequestMapping(value = "/updateProfile", method = RequestMethod.POST)
    public ModelAndView updateProfile(@Valid @ModelAttribute("profile")Profile profile, BindingResult result,Locale locale, HttpSession httpSession) {

        if(profile.getId() == null)
            return new ModelAndView("redirect:/list_profiles").addObject("error", "id == null");

        Profile testProfile = profileDBService.find1ByEmail(profile.getEmail());
        if (testProfile != null)
            if (testProfile.getId() != profile.getId())
                result.addError(new FieldError("profile", "email", "emailAlreadyTaken"));

        if (result.hasErrors())
            return new ModelAndView("root/profileFormEdit", "profile", profile);;

        profileDBService.update(profile);

        return new ModelAndView("redirect:/list_profiles");
    }
}
