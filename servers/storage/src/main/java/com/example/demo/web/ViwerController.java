package com.example.demo.web;

import com.example.demo.service.MaterialServiceDBImple;
import com.example.demo.service.ProductServiceDBImple;
import com.example.demo.service.ProfileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

@RestController
@RequestMapping("/viwer")
public class ViwerController {

    @Autowired
    private MaterialServiceDBImple materialServiceDBImple;
    @Autowired
    private ProductServiceDBImple productServiceDBImple;


    @RequestMapping(value = "/materials", method = RequestMethod.GET)
    public ModelAndView findAllMaterials(){
        return new ModelAndView("viwer/listMaterials", "listMaterials", materialServiceDBImple.findAll());
    }
    @RequestMapping(value = "/products", method = RequestMethod.GET)
    public ModelAndView findAllProducts(){
        return new ModelAndView("viwer/listProducts", "listProducts", productServiceDBImple.findAll());
    }
}
