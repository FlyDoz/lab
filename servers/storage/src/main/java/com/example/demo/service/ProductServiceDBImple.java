package com.example.demo.service;



import com.example.demo.entity.Product;
import com.example.demo.mapper.ProductsMyBatisMapper;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductServiceDBImple implements ItemService<Product> {



    private final ProductsMyBatisMapper productsMyBatisMapper;

    public ProductServiceDBImple(ProductsMyBatisMapper productsMyBatisMapper) {
        this.productsMyBatisMapper = productsMyBatisMapper;
    }

    @Override
    public List<Product> findAll() {
        return productsMyBatisMapper.findAll();
    }
    @Override
    public List<Product> findById(Long id) {
        return productsMyBatisMapper.findById(id);
    }
    @Override
    public void create(Product material) {
        productsMyBatisMapper.create(material);
    }
    @Override
    public void update(Product material){
        productsMyBatisMapper.update(material);
    }
    @Override
    public void delete(Long id){
        productsMyBatisMapper.delete(id);
    }
    @Override
    public void sellUpdate(int amount, Long id){
        productsMyBatisMapper.sellUpdate(amount, id);
    }
    public void addUpdate(int amount, Long id){
        productsMyBatisMapper.addUpdate(amount, id);
    }
}
