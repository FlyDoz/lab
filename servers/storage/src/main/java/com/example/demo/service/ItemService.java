package com.example.demo.service;


import java.util.List;

public interface ItemService<T> {
    List<T> findAll();
    List<T> findById(Long id);
    void create(T t);
    void sellUpdate(int amount, Long id);
    void update(T t);
    void delete(Long id);
}
