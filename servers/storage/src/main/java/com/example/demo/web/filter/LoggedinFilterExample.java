package com.example.demo.web.filter;


import com.example.demo.entity.Profile;
import com.example.demo.service.ProfileService;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;



public class LoggedinFilterExample implements Filter {

    public LoggedinFilterExample(ProfileService profileDBService){
        this.profileDBService = profileDBService;
    }

    private ProfileService profileDBService;

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest httpServletRequest = (HttpServletRequest) request;
        HttpServletResponse httpResponse = (HttpServletResponse) response;
        HttpSession httpSession = httpServletRequest.getSession(true);


        filterChain.doFilter(request, response);


        if(checkFreeStaticResources(httpServletRequest)) {
            try {
                Object idObj = httpSession.getAttribute("id");

                boolean loggedin = false;
                if (idObj != null) {//проверка зашол ли пользователь
                    Long id = Long.parseLong(idObj.toString());
                    if (profileDBService.profileExistsByID(id)) {
                        loggedin = true;

                        Profile profile = profileDBService.find1ByID(id);

                        //TODO rename
                        request.setAttribute("chekUser", profile.rightExists("user"));
                        request.setAttribute("chekRoot", profile.rightExists("root"));
                        //пока новое буду писать через \/
                        request.setAttribute("checkUser", profile.rightExists("user"));
                        request.setAttribute("checkRoot", profile.rightExists("root"));

                        request.setAttribute("your_name", profile.getName());

                    } else {
                        httpSession.removeAttribute("id");
                        //TODO rename
                        request.setAttribute("chekUser", false);
                        request.setAttribute("chekRoot", false);
                        //пока новое буду писать через \/
                        request.setAttribute("checkUser", false);
                        request.setAttribute("checkRoot", false);
                    }
                }

                if (checkSignInURL(httpServletRequest)) {//если на странице входа
                    if (loggedin)
                        httpResponse.sendRedirect("/");
                } else {
                    if (!loggedin)
                        httpResponse.sendRedirect("/signin");
                }

                filterChain.doFilter(request, response);
            } catch (Exception e) {
                System.err.println(e);
            }
        }else{
            filterChain.doFilter(request, response);
        }
    }

    private boolean checkSignInURL(HttpServletRequest httpServletRequest){
        String url = httpServletRequest.getServletPath();
        try {
            int beginIndex = 1;
            int endIndex = beginIndex + 6;
            url = url.substring(beginIndex,endIndex);
        } catch (Exception e){}

        return url.equals("signin");
    }


    private static String[] patterns = new String[]{
            "/img/",
            "/font/",
            "/changeLocalization",
            "/favicon.ico"
    };
    //true if do SigninFilter
    private boolean checkFreeStaticResources(HttpServletRequest httpServletRequest){
        String url = httpServletRequest.getServletPath();
        //System.out.println(url);
        for (String item: patterns)
            if(url.length() >= item.length())
                if(url.substring(0,item.length()).equals(item))
                    return false;
        return true;
    }



}
