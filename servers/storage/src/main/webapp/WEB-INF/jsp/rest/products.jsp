<%--
  Created by IntelliJ IDEA.
  User: User
  Date: 18.06.2019
  Time: 14:22
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="et" tagdir="/WEB-INF/tags/error" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
<head>
    <script type="text/javascript">

        function func1() {

            var url = "http://localhost:8081/rest/products";

            var xhr = new XMLHttpRequest();
            xhr.open('GET', url, true);
            xhr.onload = function () {
                var products = JSON.parse(xhr.responseText);
                if (xhr.readyState == 4 && xhr.status == "200") {
                    createTable(products);
                    //console.table(products);

                } else {
                    console.error(products);
                }
            };
            xhr.send(null);

        }
        function createObj() {
            var url = "http://localhost:8081/rest/products" + "?RequestMethod=POST";
            var xhr = new XMLHttpRequest();
            xhr.open("GET",
                url
                +'&name='+document.getElementById('productInput').value
                +'&amount='+document.getElementById('amountInput').value,
                true);
            xhr.onload = function () {
                var products = JSON.parse(xhr.responseText);
                if (xhr.readyState == 4 && xhr.status == "200") {
                    createTable(products);
                    //console.table(products);
                } else {
                    console.error(products);
                }
            };
            xhr.send(null);
        }
        function updateObj(id) {
            var url = "http://localhost:8081/rest/products" + "?RequestMethod=UPDATE" + "&TypeUpdate=sell";
            var xhr = new XMLHttpRequest();
            xhr.open("GET",url
                +'&id='+id
                +'&sellAmount='+document.getElementById('sellAmountInput').value
                ,true);
            xhr.onload = function () {
                var products = JSON.parse(xhr.responseText);
                if (xhr.readyState == 4 && xhr.status == "200") {
                    createTable(products);
                    //console.table(products);
                } else {
                    console.error(products);
                }
            };
            xhr.send(null);
        }
        function deleteObj(id) {
            var url = "http://localhost:8081/rest/products" + "?RequestMethod=DELETE";
            var xhr = new XMLHttpRequest();
            xhr.open("GET", url+'&id='+id, true);
            xhr.onload = function () {
                var products = JSON.parse(xhr.responseText);
                if (xhr.readyState == 4 && xhr.status == "200") {
                    createTable(products);
                    //console.table(products);
                } else {
                    console.error(products);
                }
            };
            xhr.send(null);
        }
        function createTable(products) {
            var body = document.getElementById('forTable');
            var oldTbl = document.getElementById('table1');
            if(oldTbl != null)
                body.removeChild(oldTbl);
            var tbl = document.createElement('table');
            tbl.id = "table1";
            tbl.style.width = '100%';
            tbl.setAttribute('border', '1');
            var tbdy = document.createElement('tbody');
            var tr,td;
            // tr = document.createElement('tr');
            //
            // td = document.createElement('td');
            // td.innerHTML = 'сколько';
            // tr.appendChild(td);
            //
            // td = document.createElement('td');
            // var sellAmountInput = document.createElement('input');
            // sellAmountInput.id = "sellAmountInput";
            // td.appendChild(sellAmountInput);
            // tr.appendChild(td);
            //
            // tbdy.appendChild(tr);

            tr = document.createElement('tr');
            //
            // td = document.createElement('td');
            // td.innerHTML = 'цена';
            // tr.appendChild(td);

            // td = document.createElement('td');
            // var sellCostInput = document.createElement('input');
            // sellCostInput.id = "sellCostInput";
            // td.appendChild(sellCostInput);
            // tr.appendChild(td);


            td = document.createElement('td');
            td.innerHTML = 'id';
            tr.appendChild(td);

            td = document.createElement('td');
            td.innerHTML = 'name';
            tr.appendChild(td);

            td = document.createElement('td');
            td.innerHTML = 'amount';
            tr.appendChild(td);
            tbdy.appendChild(tr);

            for (var i = 0; i < products.length; i++) {
                tr = document.createElement('tr');

                // td = document.createElement('td');
                // var sellButton = document.createElement('button');
                // sellButton.setAttribute('onclick','updateObj('+products[i].id+');');
                // sellButton.innerHTML = "продать";
                // td.appendChild(sellButton);
                // tr.appendChild(td);

                td = document.createElement('td');
                td.innerHTML = products[i].id;
                tr.appendChild(td);

                td = document.createElement('td');
                td.innerHTML = products[i].name;
                tr.appendChild(td);

                td = document.createElement('td');
                td.innerHTML = products[i].amount;
                tr.appendChild(td);

                td = document.createElement('td');
                var delButton = document.createElement('button');
                delButton.setAttribute('onclick','deleteObj('+products[i].id+');');
                delButton.innerHTML = 'X';
                td.appendChild(delButton);
                //td.innerHTML = 'X';
                tr.appendChild(td);
                tbdy.appendChild(tr);
            }
            tbl.appendChild(tbdy);
            body.appendChild(tbl);
        }

    </script>
    <title></title>
</head>
<body>



<%--    <script type="text/javascript">--%>
<%--        func1();--%>
<%--    </script>--%>
</body>
<t:mainHeadTag title="title.profileFormCreate"/>
<t:mainBodyTag>

    <div class="width200">
        <div align="right">

            <button id="btn1"
                    value="asd1"
                    onclick="func1();">
                GET products
            </button>


            <div id="forTable">
                    <%--не трогать--%>
            </div>
            <div id="forSellMenu">
                    <%--не трогать--%>
            </div>



            <table width="270">
                <tr>
                    <td>product</td>
                    <td><input id="productInput" value="productASD"/></td>
                </tr>

                <tr>
                    <td>amount</td>
                    <td><input type="number" step="1" min="1" id="amountInput" value="1"/></td>
                </tr>


<%--                <tr>--%>
<%--                    <td>date</td>--%>
<%--                    <td><input type="date" id="dateInput"/></td>--%>
<%--                </tr>--%>


                <tr><td/><td>
                    <button id="btnCreate"
                            value="asd1"
                            onclick="createObj();">
                        занести данные о произведённой продукции
                    </button>
                </td></tr>
            </table>


        </div>
    </div>

</t:mainBodyTag>
</html>
