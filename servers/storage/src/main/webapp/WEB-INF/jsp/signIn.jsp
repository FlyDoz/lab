<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://java.sun.com/jsp/jstl/fmt" %>


<html>
<t:mainHeadTag title="title.signin"/>
<t:mainBodyTag>

    <div align="center" class="width200">

        <c:out value="${error}" /> <%--TODO--%>

        <form method="post" action="/signin2">
            <input type="text" name="username" placeholder="<spring:message code="username" />" class="width200">
            <input type="password" name="password" placeholder="<spring:message code="password" />" class="width200">
<%--            <input type="checkbox" name="_spring_security_remember_me">--%>
<%--            <input type="text" name="_csrf" type="hidden">--%>
            <button type="submit"><spring:message code="signin" /></button>
        </form>
    </div>



</t:mainBodyTag>
</html>
