<%--
  Created by IntelliJ IDEA.
  User: User
  Date: 18.06.2019
  Time: 13:44
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>


<html>
<t:mainHeadTag title="title.listOfProfiles"/>
<t:table650pxTag/>
<t:mainBodyTag>

    <div align="center" class="width650">
        <div align="right">
            <c:out value="${error}" />
            <form method="post">
                <input type="text" name="findByEmail" value placeholder="<spring:message code="findByEmail" />">
            </form>
        </div>



        <table>

            <tr>
<%--                <td><spring:message code="username" /></td>--%>
<%--                <td><spring:message code="email" /></td>--%>
<%--                <td><spring:message code="rights" /></td>--%>
                <td>id</td>
                <td>название</td>
                <td>количество</td>
            </tr>

            <c:forEach var="item" items="${listMaterials}">
                <tr>
                    <td><c:out value="${item.id} "/></td>
                    <td><c:out value="${item.name} "/></td>
                    <td><c:out value="${item.amount} "/></td>
<%--                    <td>--%>
<%--                        <c:forEach var="item2" items="${item.rights}">--%>
<%--                            <c:out value="${item2}"></c:out>--%>
<%--                        </c:forEach>--%>
<%--                    </td>--%>

<%--                    <sec:authorize access="hasRole('root')">--%>
<%--                        <td>--%>
<%--                            <form method="post" action="/root/delete_profile" class="formHeight">--%>
<%--                                <button type="submit" class="x"><font color="red">X</font></button>--%>
<%--                                <input type="hidden" name="idOfProfile" value="${item.id}">--%>
<%--                            </form>--%>
<%--                        </td>--%>
<%--                    </sec:authorize>--%>



                </tr>
            </c:forEach>

        </table>



<%--        <sec:authorize access="hasRole('root')">--%>
<%--            <div align="right">--%>
<%--                <form method="get" action="/root/profileFormCreate" class="formHeight">--%>
<%--                    <button type="submit"><spring:message code="add" /></button>--%>
<%--                </form>--%>
<%--            </div>--%>
<%--        </sec:authorize>--%>


    </div>

</t:mainBodyTag>
</html>
