<%--
  Created by IntelliJ IDEA.
  User: User
  Date: 18.06.2019
  Time: 14:22
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="et" tagdir="/WEB-INF/tags/error" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
<t:mainHeadTag title="title.profileFormEdit"/>
<t:mainBodyTag>

    <div class="width200">
        <div align="right">

            <form:form method="post" action="/root/updateProfile" modelAttribute="profile">
                <table width="270">
                    <form:hidden path="id"/>


                    <et:printErrorOfFieldTRTD_TD obj="profile" field="name"/>
                    <tr>
                        <td><t:requiredFieldTag/><form:label path="name"><spring:message code="name" /></form:label></td>
                        <td><form:input path="name"/></td>
                    </tr>


                    <et:printErrorOfFieldTRTD_TD obj="profile" field="password"/>
                    <tr>
                        <td><t:requiredFieldTag/><form:label path="password"><spring:message code="password" /></form:label></td>
                        <td><form:input path="password"/></td>
                    </tr>


                    <et:printErrorOfFieldTRTD_TD obj="profile" field="email"/>
                    <tr><td/><td><c:if test="${emailAlreadyTaken}"><spring:message code="emailAlreadyTaken"/></c:if></td></tr>
                    <tr>
                        <td><t:requiredFieldTag/><form:label path="email"><spring:message code="email" /></form:label></td>
                        <td><form:input path="email"/></td>
                    </tr>


                    <et:printErrorOfFieldTRTD_TD obj="profile" field="rights"/>
                    <tr>
                        <td><form:label path="rights"><spring:message code="rights" /></form:label></td>
                        <td><form:input path="rights"/></td>
                    </tr>


                    <tr><td/><td><input type="submit" value="<spring:message code="ok"/>"/></td></tr>
                </table>
            </form:form>

        </div>
    </div>

</t:mainBodyTag>
</html>
