<%@ tag description="Simple Wrapper Tag" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<a title="<spring:message code="requiredField" />"><span style="color: red; ">*</span><a/>
<jsp:doBody />
