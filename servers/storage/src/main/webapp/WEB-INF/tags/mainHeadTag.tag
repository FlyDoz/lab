<%--
  Created by IntelliJ IDEA.
  User: User
  Date: 03.02.2020
  Time: 17:13
  To change this template use File | Settings | File Templates.
--%>
<%@ tag description="Simple Wrapper Tag" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ attribute name="title" type="java.lang.String" %>

<head>
    <title><spring:message code="${title}"/> </title>
</head>

